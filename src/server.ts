import * as Koa from "koa";
import * as Router from "koa-router";
import * as koaBody from "koa-body";
import { v1 as uuid } from "uuid";

const app = new Koa();
const router = new Router();
app.use(koaBody());

const players: Array<Player> = [];

interface Player {
  id: string;
  name: string;
}

interface GameMap {
  width: number;
  height: number;
}
interface GameUnit {
  xPos: number;
  yPos: number;
  alive: boolean;
  playerId: string;
  id: string;
}

enum ActionType {
  Move = "Move",
  Attack = "Attack"
}

interface Move {
  type: ActionType.Move;
  unitId: string;
  direction: Direction;
}
interface Attack {
  type: ActionType.Attack;
  unitId: string;
  targetId: string;
}

type Action = Move | Attack;

interface Game {
  map: GameMap;
  units: GameUnit[];
}

function createGame(width: number, height: number) {
  const map: GameMap = {
    width: width,
    height: height
  };
  const player1Unit: GameUnit = {
    xPos: 0,
    yPos: 0,
    alive: true,
    playerId: players[0].id,
    id: uuid()
  };
  const player2Unit: GameUnit = {
    xPos: map.width,
    yPos: map.height,
    alive: true,
    playerId: players[1].id,
    id: uuid()
  };
  const game: Game = {
    map: map,
    units: [player1Unit, player2Unit]
  };
  return game;
}
app.use(router.routes());

app.listen(3000);

console.log("Server running on port 3000");

function moveUnit(game: Game, action: Move) {
  let updatedGame: Game = game;
  updatedGame.units = game.units.map(unit => {
    if (action.unitId === unit.id) {
      if (action.direction === Direction.Up) {
        if (unit.yPos < 10) {
          unit.yPos += 1;
        }
      } else if (action.direction === Direction.Down) {
        if (unit.yPos > 0) {
          unit.yPos -= 1;
        }
      } else if (action.direction === Direction.Left) {
        if (unit.xPos > 0) {
          unit.xPos -= 1;
        }
      } else if (action.direction === Direction.Right) {
        if (unit.xPos < 10) {
          unit.xPos += 1;
        }
      }
    }
    return unit;
  });
  return updatedGame;
}

function attackUnit(game: Game, action: Attack) {
  let updateGame: Game = game;
  updateGame.units = updateGame.units.map(unit => {
    if (action.targetId === unit.id) {
      const shot = Math.random();
      if (shot < 0.5) {
        unit.alive = false;
      }
    }
    return unit;
  });
  return updateGame;
}
const playerTurn: ApplyPlayerAction = (game, player, actions) => {
  let updatedGame: Game = game;
  console.log(player, actions);
  actions.forEach(action => {
    if (action.type === ActionType.Move) {
      updatedGame = moveUnit(game, action);
    } else if (action.type === ActionType.Attack) {
      updatedGame = attackUnit(game, action);
    }
  });
  return updatedGame;
};

const getDistance = (myUnit: GameUnit, enemyUnit: GameUnit): number => {
  return Math.sqrt(
    Math.pow(myUnit.xPos - enemyUnit.xPos, 2) +
      Math.pow(myUnit.yPos - enemyUnit.yPos, 2)
  );
};

const eazyBot: GetPlayerAction = (game, player) => {
  const enemyUnitsId = players[0] == player ? players[1].id : players[0].id;
  const myUnitId = players[0] == player ? players[0].id : players[1].id;
  const myUnits: GameUnit[] = [];
  const enemyUnits: GameUnit[] = [];
  const actions: Action[] = [];
  game.units.forEach(unit => {
    unit.playerId == myUnitId ? myUnits.push(unit) : enemyUnits.push(unit);
  });
  for (let i = 0; i < myUnits.length; i++) {
    const myUnit = myUnits[i];
    const distances: number[] = [];
    for (let j = 0; j < enemyUnits.length; j++) {
      const enemyUnit = enemyUnits[j];
      distances.push(getDistance(myUnit, enemyUnit));
    }
    const nearestEnemy =
      enemyUnits[distances.indexOf(Math.min.apply(null, distances))];

    if (getDistance(myUnit, nearestEnemy) < 4) {
      const action: Attack = {
        type: ActionType.Attack,
        unitId: myUnit.id,
        targetId: nearestEnemy.id
      };
      actions.push(action);
    } else {
      let direction: Direction;
      if (nearestEnemy.xPos < myUnit.xPos) {
        direction = Direction.Left;
      } else if (nearestEnemy.xPos > myUnit.xPos) {
        direction = Direction.Right;
      } else if (nearestEnemy.yPos > myUnit.yPos) {
        direction = Direction.Up;
      } else if (nearestEnemy.yPos < myUnit.yPos) {
        direction = Direction.Down;
      }
      const action: Move = {
        type: ActionType.Move,
        unitId: myUnit.id,
        direction: direction
      };
      actions.push(action);
    }
  }
  return actions;
};

enum GameState {
  Done = "done",
  Running = "running",
  Draw = "draw"
}
interface GameResult {
  state: GameState;
  winner?: Player;
}

interface Game {
  map: GameMap;
  units: GameUnit[];
}

function setGameState(game: Game, turn: number, players: Player[]): GameResult {
  const player1: Player = players[0];
  const player2: Player = players[1];
  const player1Units: GameUnit[] = [];
  const player2Units: GameUnit[] = [];
  game.units.forEach(unit => {
    if (unit.playerId== player1.id && unit.alive == true) {
      player1Units.push(unit);
    } else if (unit.playerId == player2.id && unit.alive == true) {
      player2Units.push(unit);
    }
  });
  if (turn === 100) {
    return { state: GameState.Draw };
  } else if (player1Units.length == 0) {
    return { state: GameState.Done, winner: player2 };
  } else if (player2Units.length == 0) {
    return { state: GameState.Done, winner: player1 };
  } else return { state: GameState.Running };
}

const playGame: RunGame = (game, players) => {
  const whoStarts: number = Math.random();
  let actions: Action[] = [];
  let whoPlays: boolean = true;
  let turn: number = 0;
  let gameTurn: Game;
  let result: GameResult;
  const player1 = players[0];
  const player2 = players[1];
  if (whoStarts < 0.5) {
    actions = eazyBot(game, player1);
    gameTurn = playerTurn(game, player1, actions);
    whoPlays = false;
    turn++;
  } else {
    actions = eazyBot(game, player2);
    gameTurn = playerTurn(game, player2, actions);
    whoPlays = true;
    turn++;
  }
  do {
    if (whoPlays == true) {
      actions = eazyBot(gameTurn, player1);
      gameTurn = playerTurn(gameTurn, player1, actions);
      whoPlays = false;
      turn++;
    } else {
      actions = eazyBot(gameTurn, player2);
      gameTurn = playerTurn(gameTurn, player2, actions);
      whoPlays = true;
      turn++;
    }
    result = setGameState(gameTurn, turn, players);
  } while (result.state === GameState.Running);
  if (result.state == GameState.Done) {
    return result.winner;
  } else
    return {
      name: "XXX",
      id: "000"
    };
};

router.post("/start", async ctx => {
  if (players.length == 0) {
    ctx.response.body = "There are no registred players";
  } else if (players.length == 2) {
    const game = createGame(10, 10);
    const winner = playGame(game, players);
    ctx.response.body = `player ${winner.name} won the game`;
  } else {
    ctx.response.body = "Not enought players";
  }
});

router.post("/addPlayer", async ctx => {
  const player = ctx.request.body;
  players.push(player);
  ctx.response.body = `added ${player.name}, total players: ${players.length}`;
});

const actions: Action[] = [];
router.get("/actions", async ctx => {
  ctx.response.body = actions;
});

export type RunGame = (game: Game, players: Player[]) => Player;

export type DoTurn = (game: Game, player: Player, turn: number) => Game;

enum Direction {
  Up = "up",
  Down = "down",
  Left = "left",
  Right = "right"
}

export type GetPlayerAction = (game: Game, player: Player) => Action[];

export type ApplyPlayerAction = (
  game: Game,
  player: Player,
  action: Action[]
) => Game;

players.push({ name: "Radek", id: "fjijids" });
players.push({ name: "Pepa", id: "oijouhouh" });
const game = createGame(10, 10);
const winner = playGame(game, players);
console.log("Winner: ", winner);
