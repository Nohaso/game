import * as Koa from "koa";
import * as Router from "koa-router";
import * as koaBody from "koa-body";
import axios from "axios";

const app = new Koa();
const router = new Router();
app.use(koaBody());
const serverUrl:string= 'localhost:3000';

interface Player {
    ip: string;
    name: string;
    id: string;
}

enum ActionType {
    Move = 'Move',
    Attack = 'Attack'
}

const player1:Player = {
    ip:'localhost:5555',
    name: 'White Rabbit',
    id: "1"
}

function register(player:Player){   
    axios.post(`${serverUrl}/register`,player)
}  

register(player1)
router.post("/handle", async ctx => {
let action:ActionType;
    ctx.body=action;
});

app.use(router.routes())
app.listen(5555)