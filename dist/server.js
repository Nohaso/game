"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Koa = require("koa");
const Router = require("koa-router");
const koaBody = require("koa-body");
const app = new Koa();
const router = new Router();
app.use(koaBody());
const players = [];
router.post("/register", async (ctx) => {
    const player = ctx.body;
    players.push(player);
    console.log(players);
    ctx.response.message = "Hello" + player.id;
    if (players.length === 2) {
        console.log("Hra započíná");
    }
});
router.get("/", async (ctx) => {
    ctx.body = "Historie posledních 5 her:";
    console.log(ctx.body);
});
app.use(router.routes());
app.listen(3000);
console.log("Server running on port 3000");
/*
Post /register(objekt:IP,name)=>Id
get/result(historie 5 her
    2* provolá register =>spustit program
Hra: 1 Ip Post/action  body(svet)
vrátí se objekt(action) array(pohyb(objekt(typ((move,unitId,direction,)typ(attack(id,targetId))))
*/
//# sourceMappingURL=server.js.map